const matchObj = require('../public/output/matches.json');
const deliveryObj = require('../public/output/deliveries.json');
const fs = require('fs')

// Question 01
let matches = {};

for (let i in matchObj) {
    if (!matches.hasOwnProperty(matchObj[i]["season"]))
        matches[matchObj[i]["season"]] = 1;
    else
        matches[matchObj[i]["season"]] += 1;

}

fs.writeFile('../public/output/totalMatchesPerYear.json', JSON.stringify(matches, null, 4), (err) => {
    if (err) {
        throw err;
    }
});

//Question 02

function matchesWon(years) {
    let winners = {};
    for (let j in matchObj) {
        if (matchObj[j]["season"] == years) {
            if (!winners.hasOwnProperty(matchObj[j]["winner"]))
                winners[matchObj[j]["winner"]] = 1;
            else
                winners[matchObj[j]["winner"]] += 1;
        }
    }
    return winners;
}

var winners = {};

for (var z in matchObj) {
    if (!winners.hasOwnProperty(matchObj[z]["season"]))
        winners[matchObj[z]["season"]] = matchesWon(matchObj[z]["season"]);
};

fs.writeFile('../public/output/winnersPerYear.json', JSON.stringify(winners, null, 4), (err) => {
    if (err) {
        throw err;
    }
});

// Question 03

let match2016 = [];
let extraRuns = {};

for (let i in matchObj) {
    if (matchObj[i]["season"] == 2016) {
        match2016.push(matchObj[i]["id"])
    }

};

for (let j in deliveryObj) {
    if (match2016.includes(deliveryObj[j]["match_id"])) {
        if (!extraRuns[deliveryObj[j]["bowling_team"]]) {
            extraRuns[deliveryObj[j]["bowling_team"]] = parseInt(deliveryObj[j]["extra_runs"])

        } else {
            extraRuns[deliveryObj[j]["bowling_team"]] += parseInt(deliveryObj[j]["extra_runs"])

        }
    }

};

fs.writeFile('../public/output/extraRuns.json', JSON.stringify(extraRuns, null, 4), (err) => {
    if (err) {
        throw err;
    }
});

// Question 04

let match2015 = [];
let economic = {};

for (let i in matchObj) {
    if (matchObj[i]["season"] == 2015) {
        match2015.push(matchObj[i]["id"])
    }
};

function overs(bowler) {
    let balls = 0;
    for (let j in deliveryObj) {
        if (match2015.includes(deliveryObj[j]["match_id"])) {
            if (deliveryObj[j]["bowler"] == bowler) {
                balls += 1;
            }

        }
    }
    return balls / 6;
}

for(let k in deliveryObj){
    if (match2015.includes(deliveryObj[k]["match_id"])) {
        if(!economic[deliveryObj[k]["bowler"]]){
            economic[deliveryObj[k]["bowler"]] = parseInt(deliveryObj[k]["total_runs"])
        }else{
            economic[deliveryObj[k]["bowler"]] += parseInt(deliveryObj[k]["total_runs"])
        }
    }
}

for(let a in economic){
    economic[a]= Math.round((economic[a]/overs(a)));
}
let economicPlayer= Object.keys(economic).map(function(k){
    return [k, economic[k]];
} ).sort(function(a,b){return a[1]-b[1]});

let res=[];
for(let i=0; i<10; i++){
res.push(economicPlayer[i])
}

fs.writeFile('../public/output/economicPlayer.json', JSON.stringify(Object.fromEntries(res), null, 4), (err) => {
        if (err) {
            throw err;
        }
    });