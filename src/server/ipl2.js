function matchPlayed(matches) {
    return matches.map((matches) => matches.season).reduce((result, season) => {
        if (result[season]) {
            result[season] += 1;
        } else {
            result[season] = 1;
        }
        return result

    }, {})
}
//Number of matches won per team per year in IPL.
function matcheswon(match) {
    return match.reduce((result, matches) => {
        let season = matches.season;
        let winner = matches.winner;
        if (!result[season]) {
            result[season] = {};
        }
        result[season][winner] = result[season][winner] + 1 || 1
        return result;
    }, {});
}


//Extra runs conceded per team in the year 2016
function extraruns(matches, deliveries, year) {
    let matchid = [];
    matches.filter((matches) => {
        if (matches.season.includes(year)) {
            matchid.push(matches.id)
        }
    })
    let result = deliveries.reduce((res, deliveries) => {
        let match_id = deliveries.match_id;
        let extraruns = deliveries.extra_runs;
        if (matchid.includes(match_id)) {
            if (!res[deliveries.bowling_team]) {
                res[deliveries.bowling_team] = parseInt(extraruns);
            } else {
                res[deliveries.bowling_team] += parseInt(extraruns);
            }

        }
        return res;

    }, {})
    return result;

}

//Top 10 economical bowlers in the year 2015

function economicalbowler(matches, deliveries, year) {
    let matchid = [];
    matches.filter((matches) => {
        if (matches.season.includes(year)) {
            matchid.push(matches.id)
        }
    })
    function overs(bowler) {
        let balls_over = deliveries.reduce((balls, match) => {
            let match_id = match.match_id;
            if (matchid.includes(match_id)) {
                if (match.bowler == bowler) {
                    balls += 1;
                }
            }
            return balls

        }, 0)
        return balls_over / 6;
    }
    let result = deliveries.reduce((economic, deliveries) => {
        let match_id = deliveries.match_id;
        let totalruns = deliveries.total_runs;
        if (matchid.includes(match_id)) {
            if (!economic[deliveries.bowler]) {
                economic[deliveries.bowler] = parseInt(totalruns);
            } else {
                economic[deliveries.bowler] += parseInt(totalruns);
            }
        }
        return economic;
    }, {})

    result = Object.entries(result)
        .map((key) => [key[0], Math.round(key[1] / overs(key[0]))])
        .sort((a, b) => a[1] - b[1])
        .slice(0, 10);

    return Object.fromEntries(result);

}


module.exports = { matchPlayed, matcheswon, extraruns, economicalbowler }